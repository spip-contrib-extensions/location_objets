<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/location_objets.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'location_objets_description' => 'Gère la location d’objets',
	'location_objets_nom' => 'Location d’objets',
	'location_objets_slogan' => 'Louer vos objets'
);
